import React from "react";
import { Card, Button, CardTitle, CardText } from "reactstrap";

export default function Post({ title, content }) {
  return (
    <div>
      <Card body inverse color="warning" id="post">
        <CardTitle tag="h5">{title}</CardTitle>
        <CardText>{content}</CardText>
        <Button color="secondary" id="post-button">
          Click Me!
        </Button>
      </Card>
    </div>
  );
}

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import React from "react";

import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";
import Blog from "./Screens/Blog";
import { Navbar, NavbarBrand, NavItem, NavLink, Nav } from "reactstrap";

function App() {
  return (
    <Router>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Home</NavbarBrand>
        <Nav className="mr-auto" navbar>
          <NavItem>
            <NavLink href="/Profile">Profile</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/Contact-us">Contact Us</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/Blog">Blog</NavLink>
          </NavItem>
        </Nav>
      </Navbar>

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/Profile" />
        <Route component={ContactUs} path="/Contact-us" />
        <Route component={Blog} path="/Blog" />
      </Switch>
    </Router>
  );
}

export default App;

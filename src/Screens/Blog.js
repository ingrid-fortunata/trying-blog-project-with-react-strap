import React from "react";
import Post from "../Components/Post";

export default function Blog() {
  const posts = [
    { title: "Judul 1", content: "blablablabla" },
    { title: "Judul 2", content: "blablablabla" },
    { title: "Judul 3", content: "blablablabla" },
  ];

  return (
    <div>
      <h1> Ini Blog Page</h1>
      {posts.map((post) => (
        <Post title={post.title} content={post.content} />
      ))}
    </div>
  );
}

import React from "react";
import { Col, Button, Form, FormGroup, Label, Input } from "reactstrap";

const ContactUs = () => {
  return (
    <Form>
      <FormGroup row>
        <Label for="Name" sm={2}>
          Full Name:
        </Label>
        <Col sm={10}>
          <Input
            type="text"
            name="name"
            id="Name"
            placeholder="enter your name"
          />
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="Email" sm={2}>
          Email:
        </Label>
        <Col sm={10}>
          <Input
            type="email"
            name="email"
            id="Email"
            placeholder="enter your email"
          />
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="Text" sm={2}>
          Your message:
        </Label>
        <Col sm={10}>
          <Input type="textarea" name="text" id="Text" />
        </Col>
      </FormGroup>
      <FormGroup check row>
        <Col sm={{ size: 10, offset: 2 }}>
          <Button>Submit</Button>
        </Col>
      </FormGroup>
    </Form>
  );
};

export default ContactUs;
